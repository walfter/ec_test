<?php


namespace App\Imports;


final class ImportCsv
{
    /**
     * Собирает информацию из csv файла в единый массив
     * @param string $path
     * @param string $col_delimiter
     * @param false $with_header
     * @return array
     */
    public static function getRows(string $path, $col_delimiter = ',', $with_header = false): array
    {
        if (!file_exists($path)) {
            return [];
        }
        $csv_content = file_get_contents($path);
        $row_delimiter = str_contains($csv_content, "\r\n") ? "\r\n" : "\n";

        $csv_lines = explode($row_delimiter, $csv_content);

        $csv_data = [];
        $header = null;
        foreach ($csv_lines AS $line) {
            $cols = explode($col_delimiter, $line);
            if ($with_header && !$header) {
                $header = array_map('mb_strtolower', $cols);
                continue;
            }
            $csv_data[] = $header ? array_combine($header, $cols) : $cols;
        }
        return $csv_data;
    }
}
