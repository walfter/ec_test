<?php

namespace App\Filters;

use Illuminate\Database\Eloquent\Builder;

abstract class BaseFilter
{
    /** @var Builder запрос для фильтрации */
    protected Builder $builder;
    /** @var array|null список фильтров если нулл возьмётся filters из запроса */
    protected ?array $filters;

    /** @var array Список автоматических фильтров, которые можно сразу применять функцией setWhereByValue */
    protected array $auto_filters = [];

    /** @var array Операторы для выборки в запросе */
    public const OPERATORS = [
        'gt' => '>',
        'gte' => '>=',
        'lt' => '<',
        'lte' => '<=',
        'eq' => '=',
        'neq' => '<>',
        'between' => 'between',
        'like' => 'LIKE',
        'null' => 'null',
    ];

    public function __construct(?array $filters = null)
    {
        $this->filters = $filters;
    }

    /**
     * Добавляет фильтры для автоматической фильтрации
     * @param mixed ...$filters
     * @return $this
     */
    public function addAutoFilters(...$filters): self
    {
        foreach ($filters AS $filter) {
            if (!in_array($filter, $this->auto_filters)) {
                $this->auto_filters[] = $filter;
            }
        }

        return $this;
    }

    /**
     * Применяет фильтр
     * @param Builder $builder
     * @return Builder
     */
    public function apply(Builder $builder): Builder
    {
        $this->builder = $builder;
        foreach ($this->getFilters() as $filter => $value) {
            if (method_exists($this, $filter)) {
                $this->{$filter}($value);
            } elseif (in_array($filter, $this->auto_filters)) {
                $this->setWhereByValue($this->builder, $filter, $value);
            }
        }
        return $this->builder;
    }

    /**
     * Возвращает список фильтров
     * @return array
     */
    protected function getFilters(): array
    {
        $request_filters = request('filters', []);
        return $this->filters ?? $request_filters;
    }

    /**
     * Устанавливает выборку по колонке согласно оператору в значении
     *     - Если в качестве значения передаётся массив то в нём ожидается оператор и значение
     *     - Если в качестве значения передаётся строка то ожидается что оператор и значение разделены '.'
     *     - Если используется оператор between то ожидается что значения разделены -
     *     - Примеры строковых значений:
     *         - gte.1000 = больше или равно 1000
     *         - between.1000-1500 = между 1000 и 1500
     *         - like.value = похоже на value
     *
     *
     * @param Builder $builder
     * @param string $column
     * @param string|array $value_raw
     */
    protected function setWhereByValue(Builder $builder, string $column, $value_raw): void
    {
        $values = $value_raw;
        if (!is_array($value_raw) && str_contains($value_raw, '.'))
            $values = explode('.', $value_raw);

        $operator = null;
        $value = null;
        foreach ($values AS $item) {
            if (is_array($item)) {
                $this->setWhereByValue($builder, $column, $item);
            } else if (array_key_exists($item, static::OPERATORS)) {
                $operator = static::OPERATORS[$item];
            } else {
                $value = $item;
            }
        }

        if (is_null($operator) || is_null($value)) {
            return;
        }

        if ($operator === 'between') {
            if (!is_array($value)) {
                $value = explode('-', $value);
            }
            $builder->whereBetween($column, $value);
        } elseif ($operator === 'null') {
            $builder->whereNull($column);
        } else {
            if ($operator === 'LIKE' && (!str_contains($value, '_') && !str_contains($value, '_'))) {
                $value = '%'.$value.'%';
            }
            $builder->where($column, $operator, $value);
        }
    }
}
