<?php


namespace App\Filters;


final class HouseFilter extends BaseFilter
{
    protected array $auto_filters = [
        'name',
        'price',
        'bedrooms',
        'bathrooms',
        'storeys',
        'garages'
    ];
}
