<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\HouseResource;
use App\Models\House;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class HouseController extends Controller
{
    /**
     * GET /api/houses
     * @param Request $request
     * @return JsonResource
     */
    public function index(Request $request): JsonResource
    {
        $houses_query = House::query();
        if ($filters = $request->get('filters')) {
            $houses_query->filter($filters);
        }
        $houses = $houses_query->paginate();
        return (new JsonResource($houses));
    }
}
