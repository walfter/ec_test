<?php

namespace App\Models;

use App\Filters\HouseFilter;
use http\Exception\BadUrlException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class House extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'price',
        'bedrooms',
        'bathrooms',
        'storeys',
        'garages'
    ];

    protected $casts = [
        'name' => 'string',
        'price' => 'integer',
        'bedrooms' => 'integer',
        'bathrooms' => 'integer',
        'storeys' => 'integer',
        'garages' => 'integer'
    ];

    /**
     * Scopes
     */

    /**
     * Фильтрация домов
     * @param Builder $builder
     * @param array $filters
     * @return Builder
     */
    public function scopeFilter(Builder $builder, array $filters = []): Builder
    {
        return (new HouseFilter($filters))->apply($builder);
    }
}
