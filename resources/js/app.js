import Vue from 'vue';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

require('./bootstrap');
window.Vue = require('vue');

Vue.use(ElementUI);
Vue.component('houses-component', require('./components/HousesComponent.vue').default);

const app = new Vue({
    el: '#app'
});
