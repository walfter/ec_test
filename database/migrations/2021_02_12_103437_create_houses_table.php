<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('houses', function (Blueprint $table) {
            $table->id();
            $table->string('name')->index();
            $table->unsignedInteger('price')->index();
            $table->unsignedSmallInteger('bedrooms')->index();
            $table->unsignedSmallInteger('bathrooms')->index();
            $table->unsignedSmallInteger('storeys')->index();
            $table->unsignedSmallInteger('garages')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('houses');
    }
}
