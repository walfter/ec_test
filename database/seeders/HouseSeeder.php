<?php

namespace Database\Seeders;

use App\Imports\ImportCsv;
use App\Models\House;
use Illuminate\Database\Seeder;

class HouseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csv_data = ImportCsv::getRows(storage_path('imports/import.csv'), ',', true);

        foreach ($csv_data AS $line) {
            House::updateOrCreate(['name' => $line['name']], $line);
        }
    }
}
